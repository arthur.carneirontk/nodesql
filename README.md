# REST API Node, Express e MySql

Criação de uma API REST utilizando NodeJS, Express e MySQL

### Copyright (c)

---

Arthur Carneiro da Rocha Menezes

### Description

---

Este repositorio se refere ao código do projeto, suas dependências e funções.

### Hierarchy

---

##### Endpoints

- (**POST**) /atendimentos - responsável por criar o usuário no banco de dados.
- (**GET**) /atendimentos - responsável por listar os usuários que estão contidos no banco de dados.
- (**PATCH**) /atendimentos/:id - responsável por alterar valores para o id desejado.
- (**DELETE**) /atendimentos/:id - reponsável por deletar os dados do id desejado.

### Prerequisites

---

Possuir NodeJS em sua versão LTS, npm/yarn, MySql Server e MySql Workbench.

### Installing

---

```javascript
npm install
```

### Usage
---

- O Endpoint /atendimentos (**POST**) recebe como parametro [{"cliente", "pet", "servico", "status", "observacoes", "data" ]

- o Endpoint /atendimentos/:id (**PATCH**) recebe como parametro um dos dados acima, porem com base no que será editado.

### Build With
---
````
NodeJS
MySql Server e Workbench
Express
Nodemon
BodyParser
Consign
Moment
````
 - Todas são bibliotecas do NodeJs
---