const Atendimento = require('../models/atendimentos');

module.exports = app => {

    app.get('/atendimentos', (req, res) => {
        Atendimento.lista(res);
    });

    app.get('/atendimentos/:id', (req, res) => {

        const id = parseInt(req.params.id);

        Atendimento.buscaPorId(id, res);

    });

    app.post('/atendimentos', (req, res) => {

        const atendimento = req.body;
        console.log("Atendimento enviado")
        Atendimento.adiciona(atendimento, res);
    })

    app.patch('/atendimentos/:id', (req, res) => {
        const id = parseInt(req.params.id);
        const values = req.body;

        Atendimento.altera(id, values, res);
    })

    app.delete('/atendimentos/:id', (req, res) => {
        let id = req.params.id;
        console.log('id:', id)
        if (id.length <= 2) {
            console.log("Foi")
        } else {
            console.log("Passou", id.length)
        }
        Atendimento.deleta(id, res);
    });
}