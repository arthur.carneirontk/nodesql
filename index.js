const customExpress = require('./config/customExpress');
const connection = require('./infraestrutura/connection');
const Tables = require('./infraestrutura/Tables');

connection.connect(erro => {
    if (erro) {
        console.log(erro)
    } else {
        
        console.log('Conectado com sucesso!');
        
        Tables.init(connection)
        const app = customExpress();

        let PORT = 3000;
        app.listen(PORT, () => console.log('Servidor rodando na porta', PORT));

    }
});

